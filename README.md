# Tema2-SSC

   Pasii pentru a crea o masina virtuala in Azure Cloud folosind Azure CLI. <br>
Masina este conectata prin SSH cu masina mea de CentOS din VMware Workstation 
15 Player, initial am avut o masina pe Virtual Box dar am intampinat cateva 
probleme si am folosit VMWare Workstation.
*  az login   
 => logare pe contul de Azure Cloud
 => subscriptie gratis pentru o luna cu 200 $ pentru test 
*  az group create --name Tema2-SSC --location westeurope
 => creare unui Resource Group cu numele de Tema2-SSC
*  az network nsg create -n Tema2-SSC-NSG -g Tema2-SSC
 => creare un Network Security Group
*  az network nsg rule create --nsg-name Tema2-SSC-NSG -g Tema2-SSC -n \
*  Tema2-SSC-Regula --priority 100 --protocol Tcp --destination-port-ranges 22 80
 =>  Creare o regula pt Grupul de security ce accepta porturile 22 si 80
*  az vm create -n Tema2-SSC-VM -g Tema2-SSC --image centos --size Standard_B1ls \
*  --admin-username ansible --ssh-key-values ~/.ssh/id_rsa.pub --nsg Tema2-SSC-NSG
 => creare o masina virtuala de CentOS cu scopul de a o utiliza cu Ansible pe 
Azure Cloud
 => am creat o cheie ssh  (ssh-keygen) in masina mea virtuale de CentOS din 
VMware Workstation si am preluat-o cu comanda SCP, am pus-o pe laptopul meu in
fisierul .ssh si folosind PowerShell si am creat noua masina virtuale pe Cloud
ce poate fi accesata din masina virtuala din VMware Workstation.
*  az vm list --output table
=> afisare masina virtuala
*  ssh moalfamaria@192.168.56.128
=> am dat un ip addr pe masina mea de CentOS din WMware Workstation pentru a afla
Ip-ul public. Am accesat masina prin PowerShell deoarece nu am copy paste-ul 
implementat in masina de CentOS fara GUI.


In masina virtuala de CentOS (VMware Workstation) am urmarit urmatorii pasi:

- am creat un folder intitulat Tema2-SSC (mkdir)
- am creat un playbook in ansible cu un play ce defineste instanta 
 de host drept masina de CentOS creata pe Azure Cloud
- am instalat un modul de Apache in play
- am oferit permisiuni 644 catre fisierului index.html
- Serviciul Apache ruleaza si este initializat la boot
- am executat playbook-ul
- verificarea intrand pe link-ul: http://51.144.4.176/

